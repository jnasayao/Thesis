import random
import datetime
import copy
import numpy as np
import pickle as pkl
from multiprocessing import Pool
import time


def generate_data(data,samplesize, number):
    for n in range(number):
        d = get_random(data, samplesize)
        out = open("../Data/normsynssh5_cut_samp/%s/%s_%s.pkl"%(samplesize, samplesize, n), "wb")
        pkl.dump(d, out)
        out.close()

def get_random(data, samplesize):
    new = {}
    d = copy.copy(data)
    for n in range(samplesize):
        ran = np.random.choice(d.keys())
        new[ran] = d[ran]
        d.pop(ran)
    return new
        

def tile_timeseries(n1, n2):
    s1= copy.copy(n1)
    s2 = copy.copy(n2)
    s2.reverse()
    t1 = np.tile(s1, (len(s2), 1))
    t2 = np.tile(np.transpose([s2]), (1, len(s1)))
    return t1,t2

def DTWarping(n1, n2):
    w = np.int(np.round(len(n1)*0.10))
    t1,t2 = tile_timeseries(n1,n2)
    DTW = np.zeros((len(n1)+1, len(n2)+1))
    diff = np.zeros((len(n1), len(n2)))
    DTW[-1,:] = np.inf
    DTW[:,0]= np.inf
    DTW[len(n2),0] = 0
    dist = ((t1-t2)**2)
    for i in reversed(range(0,len(n1))):
        for j in range(1,len(n2)+1):
            included = range(max(1, (len(n1)-i)-w),min(len(n2)+1, (len(n1)-i)+w))
            if j in included:
		prev = min(DTW[i+1, j],DTW[i, j-1], DTW[i+1, j-1])
                DTW[(i, j)] = dist[i,j-1] + prev
		diff[(i,j-1)] = prev
            else:
                DTW[(i,j)]=np.inf
		diff[(i,j-1)] = np.inf
    return DTW, dist,diff


def get_DTWPath(n1,n2):
    DTW, dist,diff = DTWarping(n1,n2)
    warp_point = (0, len(n1)-1)
    path = []
    path.append(warp_point)
    while warp_point != (len(n1)-1,0):
        diff_element = diff[warp_point]
        row = warp_point[0]
        column = warp_point[1]
        prev = [point for point in [(row+1, column), (row+1, column+1), (row, column)] if DTW[point]==diff_element][0]
	prev = (prev[0], prev[1]-1)
        path.append(prev)
        warp_point=prev
    return path

def DTWDistance(DTW, s1, s2):
    return np.sqrt(DTW[0, len(s2)]) 

def DTW_kmeans(samplesize):#k, samplesize):
    #samplesize = 250
    k=5
    sample_no=0
    start = time.time()
    data = pkl.load(open("../Data/time_sample/uncut/%s_%s.pkl"%(samplesize,sample_no)))
    #data = pkl.load(open("../Data/normsynssh5_cut_samp/%s/%s_%s.pkl"%(samplesize,samplesize,sample_no)))
    choices = [data[label][data[label].keys()[0]].values() for label in data.keys()]
    centroids = []
    for n in range(k):
        random_seed = random.choice(choices)
        centroids.append(random_seed) 
        choices = [point for point in choices if point != random_seed] 
    print "Clustering.."
    clusters = clustering(samplesize,centroids, data)#sample_no
    out = open("../Data/kmeans/computation_time/onegram_k%ss%s_%s.pkl" %(k,samplesize,sample_no), "wb")   
    #out = open("../Data/kmeans/DTW_k5/%s/onegram_k%ss%s_%s.pkl" %(samplesize,k,samplesize,sample_no), "wb")   
    pkl.dump(clusters, out)
    out.close()
    end=time.time()
    out1 = open("../Data/kmeans/computation_time/times%s_%sk%s.pkl"%(samplesize,sample_no,k), "wb")
    #out1 = open("../Data/kmeans/DTW_k5/%s/times%s_%sk%s.pkl"%(samplesize,samplesize,sample_no,k), "wb")
    dct = {"time": (end/3600.)-(start/3600.), "k":k, "samplesize":samplesize}
    pkl.dump(dct, out1)
    out1.close()



def pick_centroids(k_clusters,data, distance="euclidean"):
    choices = [data[label][data[label].keys()[0]].values() for label in data.keys()]
    centroids = []
    initial = random.choice(choices)
    centroids.append(initial)
    choices.remove(initial)
    counter = 0
    start =  str(datetime.datetime.now())
    print counter, start
    for n in range(k_clusters-1):
        candidates = {}
        for datum in choices:
            dist = {}
            for centroid in centroids:
                if distance == "euclidean":
                    dist[tuple(centroid)] = euclid_dist(centroid, datum)
                else:
                    DTW = DTWarping(point, datum)[0]
                    dist[tuple(centroid)] = DTWDistance(DTW, point, datum)
            score = min(dist.values())
            candidates[tuple(datum)] = score
        chosen = [list(k) for k,v in candidates.iteritems() if v==max(candidates.values())][0]
        centroids.append(chosen)
        choices.remove(chosen)
        counter+=1
        print counter, start
    return centroids


def kmeans(k, data):
    centroids = pick_centroids(k, data)
    clusters = clustering(centroids, data)
    out = open("../Data/kmeans/k_2grams%s.pkl" %(k_clusters), "wb")
    pkl.dump(clusters, out)
    out.close()


def update_centroid(timeseries, labels):
    points = sorted([timeseries[label][timeseries[label].keys()[0]].values() for label in labels])
    M = copy.copy(points[0])
    for n in range(1,len(points)):
        P_i = points[n]
        W = get_DTWPath(M,points[n])
        M_tiled, P_i = tile_timeseries(M, P_i)
        for m in range(len(M)): 
            W_m1 = [i for i in W if i[1]==m]
            Q = [P_i[x] for x in W_m1] 
            M[m] = M[m] + (sum(Q)*1./len(Q)) 
    M = np.array(M)
    centroid = list(M*1./len(points))
    return tuple(centroid)

def clustering(sample_no,init_centroids, data):
    clusters = {}
    for centroid in init_centroids:
        clusters.setdefault(tuple(centroid), [])
    counter = 0
    start = str(datetime.datetime.now())
    print counter, start
    centroids = clusters.keys()
    updated_centroids = []
    while set(centroids) != set(updated_centroids):
        pcount = 0
        centroids = clusters.keys()
        d = 0
        for label in data.keys():
            datum = data[label][data[label].keys()[0]].values()
            dist = {}
            for centroid in centroids:
                DTW = DTWarping(centroid, datum)[0]
                dist[tuple(centroid)] = DTWDistance(DTW, centroid, datum)
            cluster = random.choice([k for k,v in dist.iteritems() if v == min(dist.values())])
            if label not in clusters[cluster]:
                clusters[cluster].append(label)
            for centroid in clusters.keys():
                if label in clusters[centroid]:
                    if centroid != cluster:
                        pcount += 1
                        clusters[centroid].remove(label)
            d+=1
        for centroid in clusters.keys():
            if clusters[centroid]:
                updated_centroid = update_centroid(data, clusters[centroid])
                if centroid != updated_centroid:
                    clusters[updated_centroid] = clusters[centroid]
                    clusters = {key:value for key,value in clusters.iteritems() if key!= centroid}
        updated_centroids = clusters.keys()
        print pcount, len(centroids), sample_no, start, str(datetime.datetime.now())
        print set(centroids) == set(updated_centroids)
    return clusters

def DTWSSE(data, clusters_data):
    SSE = sum([sum([(DTWDistance(DTWarping(centroid,data[label][data[label].keys()[0]].values())[0], centroid, data[label][data[label].keys()[0]].values()))**2 for label in clusters_data[centroid]]) for centroid in clusters_data.keys()])
    return SSE

#null_list = range(5,11)
#slist = range(50,1000, 50)
#if __name__ == '__main__':
#    p = Pool(processes=7)
#    p.map(DTW_kmeans, slist)



#for k in range(5,16, 5):
#dct = {}
#for samplesize in range(10,500,50):
#    dct[samplesize] = DTW_kmeans(4,samplesize)
#out = open("../Data/kmeans/DTW/runtime_sampk%s.pkl"%(10), "wb")
#pkl.dump(dct, out)
#out.close()

