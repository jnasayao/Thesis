from nltk.corpus import wordnet as wn
from collections import Counter
import datetime
import pickle
import re


def getSynsets():
    """
    Returns a list of all synsets with more than one lemma from Wordnet.
    """
    synsList = list(wn.all_synsets())
    return [str(syns.name()) for syns in synsList if len(syns.lemmas())>1]

def getlemmatuple(lemma, synsname):
    """

    Gives a tuple containing the lemma name and its pos tag.
    
    Parameters:
    --------------
    lemma: lemma object. (From synsetobject.lemma())
    synsname:  str(synsetobject.name())
    
    Returns:
    --------------
    ("lemma", "pos")
    """
    delims = ["-", "_"]
    regex = '|'.join(map(re.escape, delims))
    if str(wn.synset(synsname).pos())=='s':
        pos = 'a'                               #converts all satellite adjective pos tags to adjective.
    else:
        pos = str(wn.synset(synsname).pos())
    return ("_".join(re.split(regex, str(lemma.name()))),pos)


def getLemmas(synsname):
    """
    Gets the lemmas of a certain synset.
    
    Parameters:
    --------------
    synsname:  str(synsetobject.name())
    
    Returns:
    --------------
    lemlist: list of tuples each containing a lemma and its postag.
    """
    lemlist = []
    for lem in wn.synset(synsname).lemmas():
        lemma = getlemmatuple(lem, synsname)
        lemlist.append(lemma)
    return lemlist

def getSharedLemmas():
    """
    Gets the shared lemmas for all synsets.
    
    This function produces the sharedlemmas.pkl file
    """
    synsList = getSynsets()
    lemsyns = {}
    sharedLemmas = {}
    counter = 0
    print counter, str(datetime.datetime.now())
    for synsname in synsList:
        for lem in wn.synset(synsname).lemmas():
            lemma = getlemmatuple(lem, synsname)
            lemsyns.setdefault(lemma, []).append(synsname)
        counter += 1
    for lempos in lemsyns.keys():
        if len(lemsyns[lempos]) > 1:
            sharedLemmas[lempos] = lemsyns[lempos]
    out = open("../Data/sharedlemmas.pkl", "wb")
    pickle.dump(sharedLemmas, out)
    out.close()
    
def getsharingsyns(sharedlemmas):
    """Returns a list of all synsets who share at least one lemma"""
    sharingsyns = []
    for lemma in sharedlemmas:
        synsets = sharedlemmas[lemma]
        for synset in synsets:
            if synset not in sharingsyns:
                sharingsyns.append(synset)
    out = open("../Data/sharingsyns.pkl", "wb")
    pickle.dump(sharingsyns, out)
    out.close()
 
def getWords():
    counter=0
    all_syns = getSynsets()
    print "Synsets okay"
    words = []
    for synsname in all_syns:
        lemmas = getLemmas(synsname)
        for lemma in lemmas:
            if lemma not in words:
                words.append(lemma)
        counter+=1
        print counter, str(datetime.datetime.now())
    print "lemmas okay"
    out = open("../Data/lemmanames.pkl", "wb")
    pickle.dump(words, out)
    out.close()
    
def getSynsLem():
    """
    Returns a dictionary if synsets and corresponding lemmas.
    """
    synslem = {}
    synsets = getSynsets()
    for syns in synsets:
        synslem[syns] = getLemmas(syns)
    out = open("../Data/synslem.pkl", "wb")
    pickle.dump(synslem, out)
    out.close()
     
def is_sharing(syns):
    sharing = pickle.load(open("../Data/sharingsyns.pkl"))
    if syns in sharing:
        return True
    

def is_shared(lemmaname):
    shared = pickle.load(open("../Data/sharedlemmas.pkl"))
    return lemmaname in shared.keys()

def has_onesh(synsname):
    lemmas = getLemmas(synsname)
    lens = [len("".join(lemma[0].split("_"))) for lemma in lemmas]
    shortest_len = min(lens)
    count = Counter(lens)
    if count[shortest_len]==1:
        shortest_lemma = [lem for lem in lemmas if len("".join(lem[0].split("_")))==shortest_len][0]
        return shortest_lemma
    else:
        return False


    
