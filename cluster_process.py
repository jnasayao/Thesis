import numpy as np
import random
import pickle
import datetime
import copy

def euclid_dist(t1,t2):
    """Returns the Euclidean distance between two time series t1 and t2. 
    Parameters
    ----------
    t1: Time series 1
    t2: Time series 2
    """
    return np.sqrt(sum([(t1[n]-t2[n])**2 for n in range(len(t1))]))

def DTWarping(s1,s2):

    DTW={}
    for i in range(len(s1)):
        DTW[(i, -1)] = float('inf')
    for i in range(len(s2)):
        DTW[(-1, i)] = float('inf')
    DTW[(-1, -1)] = 0

    for i in range(len(s1)):
        for j in range(len(s2)):
            dist= (s1[i]-s2[j])**2
            DTW[(i, j)] = dist + min(DTW[(i-1, j)],DTW[(i, j-1)], DTW[(i-1, j-1)])
    return DTW

#def DTWarping(s1, s2):
#    DTW={}
#    w = 50
#    w = max(w, abs(len(s1)-len(s2)))    
#    for i in range(-1,len(s1)):
#        for j in range(-1,len(s2)):
#            DTW[(i, j)] = float('inf')
#    DTW[(-1, -1)] = 0
#  
#    for i in range(len(s1)):
#        for j in range(max(0, i-w), min(len(s2), i+w)):
#            dist= (s1[i]-s2[j])**2
#            DTW[(i, j)] = dist + min(DTW[(i-1, j)],DTW[(i, j-1)], DTW[(i-1, j-1)])
#    return DTW

def get_DTWPath(s1,s2, DTW):
    path = []
    warp_point = (len(s1)-1, len(s2)-1)
    path.append(warp_point)
    while warp_point != (0,0):
        i = copy.copy(warp_point[0])
        j = copy.copy(warp_point[1])
        dist= (s1[i]-s2[j])**2
        minimum = min(DTW[(i-1, j)],DTW[(i, j-1)], DTW[(i-1, j-1)])
        prev = [point for point in [(warp_point[0]-1, warp_point[1]), (warp_point[0], warp_point[1]-1), (warp_point[0]-1, warp_point[1]-1)] if DTW[point] == minimum][0]
        path.append(prev)
        warp_point=prev
    path = sorted(path)
    return path
    
    
def DTWDistance(s1, s2):
    DTW = DTWarping(s1,s2)
    return np.sqrt(DTW[len(s1)-1, len(s2)-1])

    
def kmeans(k, data,distance="euclidean"):
    """
    distance: "euclidean" or "DTW"
    """
    tseries =[]
    choices = []
    centroids = []
    for value in data.itervalues():
        tseries.append(value.values())
        choices.append(value.values())
    initial = random.choice(choices)
    centroids.append(initial)
    choices.remove(initial)
    counter =0
    print counter, str(datetime.datetime.now())
    for n in range(k-1):
        candidates = {}
        for datum in choices:
            dist = {}
            for point in centroids:
                if distance == "euclidean":
                    dist[tuple(point)] = euclid_dist(point, datum)
                else:
                    DTW = DTWarping(point, datum)
                    dist[tuple(point)]=DTWDistance(point ,datum)
            score = min(dist.values())
            candidates[tuple(datum)] = score
        centroid = [list(key) for key,value in candidates.iteritems() if value== max(candidates.values())][0]
        if len([list(key) for key,value in candidates.iteritems() if value== max(candidates.values())])>1:
            print "uh-oh"
        centroids.append(centroid)
        choices.remove(centroid)
        counter+=1
        print counter, str(datetime.datetime.now())
    print "Clustering.."
    clusters = clustering2(centroids, tseries, distance)
    out = open("../Data/k%s_%s.pkl" %(k, distance), "wb")
    pickle.dump(clusters, out)

def DTW_kmeans(k,tseries):
    centroids = []
    initial = random.choice(tseries)
    #initial = tseries[first]
    centroids.append(initial)
    counter =0
    print counter, str(datetime.datetime.now())
    for n in range(k-1):
        candidates = {}
        for datum in tseries:
            dist = {}
            for point in centroids:
                DTW = DTWarping(point, datum)
                dist[tuple(point)]=DTWDistance(point ,datum)
            score = min(dist.values())
            candidates[tuple(datum)] = score
        centroid = [key for key,value in candidates.iteritems() if value== max(candidates.values())][0]
        if len([list(key) for key,value in candidates.iteritems() if value== max(candidates.values())])>1:
            print "uh-oh"
        centroids.append(centroid)
        counter+=1
        print counter, str(datetime.datetime.now())
    print "Clustering.."
    clusters = clustering2(centroids, tseries, distance = "DTW")
    out = open("../Data/k%s_DTW_updated.pkl" %(k), "wb")
    pickle.dump(clusters, out)
    
    
        
def update_centroid(points, distance="euclidean"):
    if distance=="euclidean":
        centroid = [sum([p[n] for p in points])*1./len(points) for n in range(len(points[0]))]
    else:
        M = list(copy.copy(points[0]))
        for n in range(1,len(points)):
            P_i = points[n]
            DTW = DTWarping(M, P_i)
            W = get_DTWPath(M,points[n], DTW)
            for m in range(len(M)):
                W_m1 = [i[1] for i in W if i[0]==m]
                Q = [P_i[x] for x in W_m1]
                M[m] = M[m] + (sum(Q)*1./len(Q))
        centroid = [M[n]*1./len(points) for n in range(len(M))]
    return centroid
        
   # return centroid
        
        
    

def clustering1(centroids, timeseries, distance="euclidean"):
    clusters={}
    for centroid in centroids:
        clusters.setdefault(tuple(centroid), [])
        clusters[tuple(centroid)].append(centroid)
    counter = 0
    print counter, str(datetime.datetime.now())
    for datum in timeseries:
        dist = {}
        for centroid in centroids:
            if distance=="euclidean":
                dist[tuple(centroid)]=euclid_dist(centroid,datum)
            else:
                dist[tuple(centroid)]=DTWDistance(centroid,datum)
        cluster = random.choice([list(key) for key,value in dist.iteritems() if value==min(dist.values())])
        clusters[tuple(cluster)].append(datum)
        updated_centroid = update_centroid(clusters[tuple(cluster)])
        clusters[tuple(updated_centroid)] = clusters[tuple(cluster)]
        clusters = {key:value for key,value in clusters.iteritems() if key != tuple(cluster)}
        centroids.remove(cluster)
        centroids.append(updated_centroid)
        counter+=1
        print counter, str(datetime.datetime.now())
    return clusters

def clustering2(init_centroids, timeseries, distance="euclidean"):
    clusters = {}
    for centroid in init_centroids:
        clusters.setdefault(tuple(centroid), [])
    counter = 0
    print counter, str(datetime.datetime.now())
    centroids = clusters.keys()
    updated_centroids = []
    while set(centroids) != set(updated_centroids):
        pcount = 0
        centroids=clusters.keys()
        for datum in timeseries:
            dist = {}
            for centroid in centroids:
                if distance=="euclidean":
                    dist[centroid]=euclid_dist(list(centroid),datum)
                else:
                    DTW = DTWarping(list(centroid), datum)
                    dist[centroid]=DTWDistance(list(centroid), datum)
            cluster = random.choice([list(key) for key,value in dist.iteritems() if value==min(dist.values())])
            if datum not in clusters[tuple(cluster)]:            
                clusters[tuple(cluster)].append(datum)
            for centroid in clusters.keys():
                if datum in clusters[centroid]:
                    if list(centroid) != cluster:
                        pcount+=1                      
                        clusters[centroid].remove(datum)
        for centroid in clusters.keys():
            updated_centroid = update_centroid(clusters[centroid], distance)
            if centroid != tuple(updated_centroid):
                clusters[tuple(updated_centroid)] = clusters[centroid]
                clusters = {key:value for key,value in clusters.iteritems() if key != centroid}
        updated_centroids = clusters.keys()
        print pcount, len(centroids), str(datetime.datetime.now())
        print set(centroids)== set(updated_centroids)
    return clusters

#def run_k(klist, data):
#    for k in klist:
#        kmeans(k, data)


def plot_SSE(klist):
    kdict = {}
    for k in klist:
        data = pickle.load(open("../Data/kmeans/2grams/k_2grams%s.pkl" %k))
        kdict[k] = SSE(data)
    out = open("../Data/kmeans/2grams/SSE_2grams.pkl","wb")
    pickle.dump(kdict, out)
    out.close()
    
def plotDTW_SSE(klist):
    kdict = {}
    for k in klist:
        data = pickle.load(open("../Data/k%s_DTW_updated.pkl" %k))
        kdict[k] = DTWSSE(data)
    return kdict

def SSE(clusters_data):
    SSE = sum([sum([(euclid_dist(centroid, point))**2 for point in clusters_data[centroid]]) for centroid in clusters_data.keys()])
    return SSE
    
def DTWSSE(clusters_data):
    SSE = sum([sum([(DTWDistance(centroid, point))**2 for point in clusters_data[centroid]]) for centroid in clusters_data.keys()])
    return SSE

def plot_fig_2(data, clusterdict):
    f, axarr = plt.subplot(3, sharex = True)
    for x in clusterdict.keys():
        for n in clusterdict[x].keys():
            if n%2==0 and n !=0:
                lem,=axarr[x].plot(range(1507,2007), cluster1[n], "--")
            else:
                lem,=axarr[x].plot(range(1507,2007), cluster1[n])
            plotlist.append(lem)
            axarr[x].ylabel("%Frequency")
    plt.xlabel("year")
    plt.savefig("../Data/clustersubplot.png", format="png")
    plt.close()
    
    
#def get_clusterdict(data,clusterlist):
#    clusterdict = {}
#    ks = data.keys()
#    for n in len(clusterlis)t:
        
        
        

    
    
        
        
                
                
            
#        
#    
#    
#        center = random.choice(choices)
#        centroids.append(center)
#        choices.remove(center)
#    return centroids
##    for point in tseries:
#        if point not in centroids:
#            for 
#    
    
        
    
    
    

    
    

    
#def DTWDistance(s1, s2,w):
#    DTW={}
#    w = max(w, abs(len(s1)-len(s2)))
#    for i in range(-1,len(s1)):
#        for j in range(-1,len(s2)):
#            DTW[(i, j)] = float('inf')
#    DTW[(-1, -1)] = 0
#    for i in range(len(s1)):
#        for j in range(max(0, i-w), min(len(s2), i+w)):
#            dist= (s1[i]-s2[j])**2
#            DTW[(i, j)] = dist + min(DTW[(i-1, j)],DTW[(i, j-1)], DTW[(i-1, j-1)])
#    return sqrt(DTW[len(s1)-1, len(s2)-1])
