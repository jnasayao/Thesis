import numpy as np
import pickle as pkl

def randomize(dct):
    new = {}
    for syns in dct.keys():
        new[syns]={}
        for lemma in dct[syns].keys():
            year = dct[syns][lemma].keys()
            freq = dct[syns][lemma].values()
            np.random.shuffle(freq)
            new[syns][lemma] = dict(zip(year, freq))
    out = open("../Data/Randomized/normsynssh5_random.pkl", "wb")
    pkl.dump(new, out)
    out.close()



