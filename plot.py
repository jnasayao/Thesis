import pickle as pkl
import DTW
import datetime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def plot_k(k,samplesize,sample_no):
    data = pkl.load(open("../Data/kmeans/DTW_k6/%s/onegram_k%ss%s_%s.pkl"%(samplesize,k,samplesize,sample_no)))
    fig, ax = plt.subplots(1)
    for centroid in data.keys():
        ax.plot(range(1800, 2007), centroid, label = str((len(data[centroid]))))
        ax.legend(title = "No. of Members",loc=4, fancybox = True, framealpha=0.5)
    ax.set_xlim(1800,2008)
    ax.set_xlabel("Year")
    ax.set_ylabel("Relative Frequency")
    plt.savefig("../Data/kmeans/DTW_k6/%s/k%s_s%s.png"%(samplesize, k, sample_no), format="png")
    plt.close()

def plotDTW_SSE(data, klist, samplesize,sample_no):
    kdict = {}
    for k in klist:
        clusters_data = pkl.load(open("../Data/kmeans/DTW/%s/onegram_k%ss%s_%s.pkl"%(samplesize,k, samplesize,sample_no)))
        kdict[k] = DTW.DTWSSE(data, clusters_data)
        print k, str(datetime.datetime.now())
    plt.plot(kdict.keys(), kdict.values(), "go")
    plt.xlabel("Number of clusters k")
    plt.ylabel("SSE")
    plt.xlim(xmax=20)
    plt.savefig("../Data/kmeans/DTW/SSE_%s.png"%samplesize, format="png")
    plt.close()


#data = pkl.load(open("../Data/normsynssh5_cut_samp/500_0.pkl"))
