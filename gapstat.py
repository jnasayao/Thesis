import pickle as pkl
import numpy as np
import datetime
import kmeans
import copy
import scipy.stats

def Wk(k, clusters, data):
    W_k =  sum([sum([(kmeans.euclid_dist(centroid, data[point][data[point].keys()[0]].values()))**2 for point in clusters[centroid]]) for centroid in clusters.keys()])
    return W_k


def logWk(k, clusters, data):
    W_k = Wk(k, clusters, data)
    return np.log(W_k)

def plot_elbow(datalist):
    lrvalue = 0
    n=30
    dcopy = copy.copy(datalist)
    left = copy.copy(dcopy)
    right = copy.copy(dcopy)
    while (lrvalue**2)<0.95:
        lslope, lintercept, lrvalue, pvalue, stderr = scipy.stats.linregress(range(2,n), left)
        n-=1
        left.pop(-1)
    n=2
    rrvalue=0
    while (rrvalue**2)<0.95:
        rslope, rintercept, rrvalue, pvalue, stderr = scipy.stats.linregress(range(n,30), right)
        n+=1
        right.pop(0)
    return ((lslope, lintercept), (rslope, rintercept))

def sum_logWkb(k):
    summ = 0
    B=10
    for n in range(1,B+1):
        data = pkl.load(open("../Data/kmeans/null_uniform/uniform_%s.pkl"%n))
        uni_distr = pkl.load(open("../Data/kmeans/null_uniform/k%s_uniform%s.pkl"%(k,n)))
        summ += logWk(k, uni_distr, data)
    return summ

def gap(k, summ, log_Wk):
    B = 10
    gap_k = ((1./B)*summ) - log_Wk
    return gap_k

def sk(k, summ):
    B = 10
    summ = 0
    lbar = (1./B)*summ
    for n in range(1,B+1):
        data = pkl.load(open("../Data/kmeans/null_uniform/uniform_%s.pkl"%n))
        uni_distr = pkl.load(open("../Data/kmeans/null_uniform/k%s_uniform%s.pkl"%(k, n)))
        summ += (logWk(k,uni_distr, data) - lbar)**2
    sd_k = np.sqrt((1./B)*summ)
    s_k = sd_k*(np.sqrt(1+(1./B)))
    return s_k

def thresh(k_plus1, gap_plus1, summ_plus1, logWk_plus1):
    thr = gap(k_plus1, summ_plus1, logWk_plus1) - sk(k_plus1, summ_plus1)
    return thr

def pick_k(klist):
   candidates = [k for k in klist if gap(k)>=thresh(k)]
   chosen_k = min(candidates)
   return chosen_k

def run_gapstat(B, klist):
    klist.reverse()
    k_plus1 = klist[0]+1
    print k_plus1
    data = pkl.load(open("../Data/twonormsynssh5.pkl"))
    cluster_plus1 = pkl.load(open("../Data/kmeans/2grams/k_2grams%s.pkl"%k_plus1))
    logWk_plus1 = logWk(k_plus1, cluster_plus1, data)
    summ_plus1 = sum_logWkb(k_plus1)
    gap_plus1 = gap(k_plus1, summ_plus1, logWk_plus1)
    threshold = gap_plus1 - sk(k_plus1,summ_plus1)
    gap_stat = {}
    for k in klist:
        print k
        clusters = pkl.load(open("../Data/kmeans/2grams/k_2grams%s.pkl"%k))
        W_k = Wk(k, clusters, data)
        log_Wk = np.log(W_k)
        summ = sum_logWkb(k)
        gap_k = gap(k, summ, log_Wk)
        diff = gap_k - threshold
        gap_stat[k] = {"Wk": W_k, "logWk": log_Wk, "ave_logWkb": (1./B)*summ, "gap":gap_k, "diff": diff} 
        threshold = gap_k - sk(k, summ)
        print k, str(datetime.datetime.now())
    out = open("../Data/kmeans/null_uniform/gap_statB%s.pkl"%B, "wb")
    pkl.dump(gap_stat, out)
    out.close()
