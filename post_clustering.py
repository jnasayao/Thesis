import pickle as pkl
import kmeans as km
import copy 
import random
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['xtick.labelsize']=16
matplotlib.rcParams['ytick.labelsize']=16
import matplotlib.pyplot as plt
import seaborn as sns
import collections
from scipy import stats
import glob

def plot_runtime(cut):
    datalist = glob.glob("../Data/kmeans/computation_time/%s/times*"%cut)
    new = {}
    for d in datalist:
        if d[-11]!='s':
            new[int(d[-11:-8])] = pkl.load(open(d))['time']
        else:
            new[int(d[-10:-8])] = pkl.load(open(d))['time']
    print new
    poly = np.polyfit(new.keys(), new.values(), 2)
    x=np.linspace(0,1000,1000)
    print poly
    y=(poly[0]*(x**2)) +(poly[1]*x) + poly[2]
    plt.plot(x,y, color="red")
    plt.plot(new.keys(), new.values(), "bo")
    plt.xlabel("Number of samples")
    plt.ylabel("Runtime in hours")
    plt.savefig("../Data/images/run_time_%s.png"%cut, formalt="png")
    plt.close()


def plot_all(k,grouping,group_pos, samplesize):
    for group_no, group in enumerate(grouping.keys()):
        pos = group_pos[group]
        df = convert_pos_pd(pos, group_no)
        plot_boxplot(k,group_no,df, samplesize)
        rel_members = grouping[group].values()
        centroids = grouping[group].keys()
        plot_hist(k,group_no, rel_members, samplesize)
        plot_percentile(k,group_no, centroids, samplesize)
    #plt.savefig("../Data/images/rapid_percentile%s.png"%samplesize, format="png")
    #plt.close()


def plot_boxplot(k,group_no,df,samplesize):
    sns.set_palette("pastel")
    n = group_no+1
    ax = sns.boxplot(x = "POS", y="RELATIVE FREQUENCY", data=df)
    plt.ylim(ymax=0.8)
    plt.xlabel("Part of Speech", fontsize=18)
    plt.ylabel("Relative Frequency", fontsize=18)
    plt.savefig("../Data/images/n%sk%sboxplot_%s.png"%(samplesize,k,group_no), format="png")
    plt.close()

def convert_pos_pd(pos, group_no):
    final_dataset = []
    for p in pos.keys():
        pos_labels = [p]*len(pos[p])
        group_labels = ["Group_%s"%group_no]*len(pos[p])
        no_of_pos = pos[p]
        dataset = list(zip(group_labels,pos_labels, no_of_pos))
        final_dataset.extend(dataset)
    df = pd.DataFrame(data=final_dataset, columns = ["GROUP", "POS", "RELATIVE FREQUENCY"])
    return df
    
    
def plot_pos(group_no, pos, samplesize):
    sns.set_palette("GnBu_d")
    labels = pos.keys()
    x = [0.25,0.5,0.75,1]
    plt.bar(x,pos.values(),0.15, alpha=0.7, align="center")
    plt.xticks(x, labels)
    plt.ylabel("No. of centroids", fontsize=19)
    plt.xlabel("Most frequent POS tag", fontsize=19)
    plt.savefig("../Data/images/hist_pos%s.png"%(group_no,saplesize), format="png")
    plt.close()


def plot_hist(k,group_no,rel_members,samplesize):
    sns.set_palette("BuGn_r")
    plt.xlim(xmax=0.4)
    plt.ylim(ymax=35)
    plt.hist(rel_members, alpha=0.5)
    mean = np.round(np.mean(rel_members), decimals=3)
    median = np.round(np.median(rel_members),decimals=3)
    mode=np.round(stats.mode(rel_members)[0][0], decimals=3)
    plt.text(0.30,28,"Mean=%s\nMedian=%s\nMode=%s"%(mean,median,mode),bbox=dict(facecolor='white',alpha=0.5), fontsize=15)
    plt.ylabel("Number of centroids",fontsize=19)
    plt.xlabel("Relative number of members", fontsize=19)
    plt.savefig("../Data/images/n%sk%shist_group%s.png"%(samplesize,k, group_no), format="png")
    plt.close()


def plot_percentile(k,group_no,centroids, samplesize):
    yearlist = range(1800,2007)
    medianlist = np.percentile(centroids,50, axis=0)
    first_quartile = np.percentile(centroids, 25, axis=0)
    third_quartile = np.percentile(centroids, 75, axis=0)
    plt.xlabel("Year", fontsize=19)
    plt.ylabel("Relative Frequency", fontsize=19)
    plt.xlim(xmin=1800,xmax=2007)    
    plt.plot(yearlist, medianlist, color = "black", ls="solid",alpha = 0.5, lw=2)
    plt.fill_between(yearlist, first_quartile, third_quartile, color="grey", edgecolor="black", alpha = 0.5, lw=2)
    plt.savefig("../Data/images/n%sk%sgroup%s_percentile.png"%(samplesize,k,group_no), format="png")
    plt.close()
        


def convert_to_pd(grouping):
    years = range(1800,2007)
    final_dataset = []
    for group_no, group in enumerate(grouping.keys()):
        group_label = ["Group %s"%group_no]*len(years)
        data = grouping[group]
        for d in data:
            rel_freq = d
            dataset = list(zip(years, rel_freq, group_label))
            final_dataset.extend(dataset)
    df = pd.DataFrame(data = final_dataset, columns = ['Year', 'Relative Frequency', 'Group'])
    df.to_csv("../Data/kmeans/DTW_k6/grouping.csv", index=False)

def total_hist(k,samplesize,grouping,target):
    new = {}
    final = {}
    for n in target:
        new[grouping.keys()[n]] = grouping[grouping.keys()[n]]
    for n in range(0,100):
        final.setdefault(n, 0)
        data = pkl.load(open("../Data/kmeans/DTW_k%s/%s/onegram_k%ss%s_%s.pkl"%(k,samplesize,k,samplesize,n)))
        centroids = data.keys()
        grouped = group_centroids(grouping.keys(),centroids)
        for group in grouped.keys():
            if group in new.keys():
                final[n]+= new[group][grouped[group]]
    mean = np.round(np.mean(final.values()), decimals=3)
    median = np.round(np.median(final.values()), decimals=3)
    mode = np.round(stats.mode(final.values())[0][0], decimals=3)
    plot_all(k,new,samplesize)
    sns.set_palette("BuGn_r")
    plt.hist(final.values(), alpha=0.5)
    plt.text(0.63,20,"Mean=%s\nMedian=%s\nMode=%s"%(mean,median,mode),bbox = dict(facecolor='white',alpha=0.5),fontsize=15)
    plt.xlim(xmin=0.45, xmax=0.70)
    plt.ylabel("Number of centroids", fontsize=19)
    plt.xlabel("Relative Number of Members", fontsize=19)
    plt.savefig("../Data/images/rapid_hist%s.png"%samplesize, format="png")
    plt.close()
    return final



def group_all(k,samplesize, group_sampleno):
    group_dict = pkl.load(open("../Data/kmeans/DTW_k%s/%s/onegram_k%ss%s_%s.pkl"%(k,samplesize,k, samplesize, group_sampleno)))
    groups = group_dict.keys()
    grouping = {}
    for group in groups:
        grouping[group] = {}
    for n in range(0,100):
        check = 0
        data = pkl.load(open("../Data/kmeans/DTW_k%s/%s/onegram_k%ss%s_%s.pkl"%(k,samplesize,k,samplesize,n)))
        centroids = data.keys()
        grouped = group_centroids(groups, centroids)
        for group in grouped.keys():
            centroid = grouped[group]
            check+=len(data[centroid])
            grouping[group][centroid]=len(data[centroid])*1./samplesize
        print check
    out = open("../Data/kmeans/DTW_k%s/%s/grouping.pkl"%(k,samplesize), "wb")
    pkl.dump(grouping, out)
    out.close()
    return grouping
     
def get_pos(k,samplesize,grouping):
    groups = grouping.keys()
    group_pos = {}
    pos_map = {"n":"NOUNS", "v":"VERBS", "r":"ADVERBS", "a": "ADJECTIVES"}
    for n in range(0,100):
        print n
        raw = pkl.load(open("../Data/normsynssh5_cut_samp/%s/%s_%s.pkl"%(samplesize, samplesize,n)))
        clustered = pkl.load(open("../Data/kmeans/DTW_k%s/%s/onegram_k%ss%s_%s.pkl"%(k,samplesize,k,samplesize,n)))
        total_pos_count = collections.Counter([raw[synset].keys()[0][1] for synset in raw.keys()])
        centroids = clustered.keys()
        grouped = group_centroids(groups,centroids)
        for group in grouped.keys():
            print clustered[grouped[group]]
            pos_count = collections.Counter([raw[synset].keys()[0][1] for synset in clustered[grouped[group]]])
            group_pos.setdefault(group, {})
            for pos in pos_count.keys():
                group_pos[group].setdefault(pos_map[pos],[])
                group_pos[group][pos_map[pos]].append(pos_count[pos]*1./total_pos_count[pos])
    return group_pos



def group_centroids(groups, centroids):
    centroid_list = copy.copy(centroids)
    group_dist = {}
    grouping = {}
    for centroid in centroids:
        for group in groups:
            group_dist.setdefault(group, {})
            group_dist[group][centroid] = km.euclid_dist(group, centroid)
    for group in groups:
        grouping[group] = random.choice([centroid for centroid in centroid_list if group_dist[group][centroid] == min([group_dist[group][cent] for cent in centroid_list])])
        centroid_list.remove(grouping[group])
    return grouping



