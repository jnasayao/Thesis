import os
import gss
import gzip
import re
import string
import datetime
import pickle
import glob
from collections import Counter
from multiprocessing import Pool

def one_gram(char):
    occ = {}
    all_words = pickle.load(open("../Data/lemmanames.pkl"))
    pos_dict = {'NOUN': 'n', 'ADV':'r', 'ADJ':'a', 'VERB':'v'}
    delims = ["-", "_"]
    regex = '|'.join(map(re.escape, delims))
    search_words = [(re.split(regex, word[0])[0], word[1]) for word in all_words if word[0][:1]==char and len(re.split(regex, word[0]))==1]
    if search_words:
        extr_gz = gzip.open("../Data/google-ngram/1-gram/eng-all/googlebooks-eng-all-1gram-20120701-%s.gz"%(char))
        counter = 0
        for line in extr_gz:
            items = line.split('\t')
            year, occurrence = items[1], int(items[2])
            gword_pos = items[0].split('_')
            gword = string.lower(gword_pos[0])
            if len(gword_pos)==2:
                if gword_pos[1] in pos_dict.keys():
                    lemma = (gword, pos_dict[gword_pos[1]])
                    if lemma in search_words:
                        occ.setdefault(lemma,{})
                        occ[lemma][year] = occ[lemma].get(year,0) + occurrence
            counter+=1
            print counter, str(datetime.datetime.now())
        out = open("../Data/wordocc/1-gram/wordocc_%s.pkl" %(char), "wb")
        pickle.dump(occ, out)
        out.close()


def two_gram(char):
    occ = {}
    all_words = pickle.load(open("../Data/lemmanames.pkl"))
    delims = ["-", "_"]
    regex = '|'.join(map(re.escape, delims))
    words = [w[0] for w in all_words]
    wordcount = Counter(words)
    repeated = [w for w in wordcount.keys() if wordcount[w]>1]
    search_words = dict([("_".join(re.split(regex,word[0])),word[1]) for word in all_words if word[0][:2]==char and len(re.split(regex, word[0]))==2 and word[0] not in repeated])
    search_list = search_words.keys()
    if search_words:
        extr_gz=gzip.open("../Data/google-ngram/2-grams/googlebooks-eng-all-2gram-20120701-%s.gz"%(char))
        counter = 0
        for line in extr_gz:
            items = line.split('\t')
            year, occurrence = items[1], int(items[2])
            sep = items[0].split()
            gwords = []
            for word in sep:
                split_word = word.split("_")
                gwords.extend([split_word[0]])
            gword_final = string.lower("_".join(gwords))
            if gword_final in search_list:
                lemma = (gword_final,search_words[gword_final])
                occ.setdefault(lemma,{})
                occ[lemma][year] = occ[lemma].get(year,0) + occurrence
            counter+=1
            print counter, str(datetime.datetime.now())
        out = open("../Data/wordocc/2-gram/wordocc_%s.pkl" %(char), "wb")
        pickle.dump(occ, out)
        out.close()

def moving_window(wordocc, windowsize):
    """
    Smooths time series data of a given word using a moving window with
    the specified window size.

    Parameters:
    wordocc: time series dictionary.
    windowsize: Size of moving window. Must be an odd number!
    """
    averaged = {}
    yearlist = [year for year in range(1505+((windowsize-1)/2), 2008-((windowsize-1)/2)+1)]
    for year in yearlist:
        total = 0
        windowyears = range(year-((windowsize-1)/2), year + ((windowsize-1)/2)+1)
        for wyear in windowyears:
            total+=wordocc.get(str(wyear),0)
        averaged[year] = total*1./windowsize
    return averaged


def get_synsocc(windowsize, two_grams=True):
    """Gets the occurrence of the lemmas in all synsets and groups them.
    This code produces synsocc%s.pkl, normsynsocc%s.pkl and normsynssh%s.pkl %(windowsize).
    """
    synsocc = {}
    normalized = {}
    synssh = {}
    delims = ["-", "_"]
    regex = '|'.join(map(re.escape, delims))
    synslem = pickle.load(open("../Data/synslem.pkl"))
    contr_2grams = pickle.load(open("../Data/contr_2grams.pkl"))
    yearlist = [year for year in range(1505+((windowsize-1)/2), 2008-((windowsize-1)/2)+1)]
    counter = 0
    for synset in synslem.keys():
        shortest = gss.has_onesh(synset)
        if not gss.is_sharing(synset) and shortest:
            total = {}
            synsocc.setdefault(synset, {})
            not_all_zero=[]
            for orig_lem in synslem[synset]:
                lemma = ("_".join(re.split(regex, orig_lem[0])), orig_lem[1])
                if len(lemma[0].split("_"))==2 and os.path.isfile("../Data/wordocc/2-gram/wordocc_%s.pkl"%(str(lemma[0][:2]))):
                    if two_grams:
                        gdata = pickle.load(open("../Data/wordocc/2-gram/wordocc_%s.pkl"%(str(lemma[0][:2]))))
                    else:
                        gdata = {}
                    print "two"
                elif len(lemma[0].split("_"))==1 and os.path.isfile("../Data/wordocc/1-gram/wordocc_%s.pkl"%(str(lemma[0][:1]))):
                    gdata = pickle.load(open("../Data/wordocc/1-gram/wordocc_%s.pkl"%(str(lemma[0][:1]))))
                    print "one"
                else:
                    gdata = {}
                if gdata and (lemma in gdata.keys()):
                    not_all_zero.append(True)
                    averaged = moving_window(gdata[lemma], windowsize)
                    if lemma in contr_2grams:
                        tseries = {k:v/2. for k,v in averaged.iteritems()}
                    else:
                        tseries = averaged
                    synsocc[synset][orig_lem] = tseries
                else:
                    not_all_zero.append(False)
                    synsocc[synset][orig_lem] = {year:0 for year in yearlist}
                for year in synsocc[synset][orig_lem].keys():
                    total[year] = total.get(year,0) + synsocc[synset][orig_lem][year]
            if any(not_all_zero):
                print "not_all"
                for orig_lem in synslem[synset]:
                    normalized.setdefault(synset,{})
                    normalized[synset][orig_lem] = {k:(v*1./total[k] if total[k] else v) for k,v in synsocc[synset][orig_lem].iteritems()}
                    if orig_lem == shortest:
                        print "haha"
                        synssh.setdefault(synset,{})
                        synssh[synset][orig_lem] = normalized[synset][orig_lem]
            else:
                synsocc.pop(synset)
                print "POP!"
        counter+=1
        print counter, str(datetime.datetime.now())
    if two_grams:
        synsoccout = open("../Data/twosynsocc%s.pkl"%(windowsize), "wb")
        normout = open("../Data/twonormsynsocc%s.pkl"%(windowsize), "wb")
        synsshout = open("../Data/twonormsynssh%s.pkl"%(windowsize), "wb")
    else:
        synsoccout = open("../Data/synsocc%s.pkl"%(windowsize), "wb")
        normout = open("../Data/normsynsocc%s.pkl"%(windowsize), "wb")
        synsshout = open("../Data/normsynssh%s.pkl"%(windowsize), "wb")
    pickle.dump(synsocc, synsoccout)
    synsoccout.close()
    pickle.dump(normalized, normout)
    normout.close()
    pickle.dump(synssh, synsshout)
    synsshout.close()
                        
                            
                    



            
    



#filelist = glob.glob("../Data/google-ngram/1-gram/eng-all/google*")
#delims = ["-", "."]
#regex = '|'.join(map(re.escape, delims))
#charlist = [re.split(regex,n)[10] for n in filelist]
#filechars = [re.split(regex,n)[10] for n in filelist] 
#charlist = [n for n in filechars if n in string.ascii_lowercase]

#if __name__ == '__main__':
#    p = Pool(processes=4)
#    p.map(one_gram, charlist)
	
		
	
