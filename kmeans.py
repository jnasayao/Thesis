import random
import datetime
import copy
import numpy as np
import pickle as pkl
from multiprocessing import Pool
import time

def euclid_dist(t1,t2):
    """Returns the Euclidean distance between two time series t1 and t2..
    Parameters
    ----------
    t1: Time series 1
    t2: Time series 2
    """
    t1 = np.array(t1)
    t2 = np.array(t2)  
    return np.sqrt(np.sum((t1-t2)**2))

def tile_timeseries(n1, n2):
    s1= copy.copy(n1)
    s2 = copy.copy(n2)
    s2.reverse()
    t1 = np.tile(s1, (len(s2), 1))
    t2 = np.tile(np.transpose([s2]), (1, len(s1)))
    return t1,t2

def DTWarping(n1, n2):
    w = np.int(np.round(len(n1)*0.10))
    t1,t2 = tile_timeseries(n1,n2)
    DTW = np.zeros((len(n1)+1, len(n2)+1))
    DTW[-1,:] = np.inf
    DTW[:,0]= np.inf
    DTW[len(n2),0] = 0
    dist = ((t1-t2)**2)
    for i in reversed(range(0,len(n1))):
        for j in range(1,len(n2)+1):
            included = range(max(1, (len(n1)-i)-w),min(len(n2)+1, (len(n1)-i)+w))
            if j in included:
                DTW[(i, j)] = dist[i,j-1] + min(DTW[i+1, j],DTW[i, j-1], DTW[i+1, j-1])
            else:
                DTW[(i,j)]=np.inf
    return DTW, dist

def get_index(tupl):
    return [np.array([tupl[0]]), np.array([tupl[1]])]

def get_DTWPath(n1,n2):
    DTW, dist = DTWarping(n1,n2)
    DTW_cut = np.delete(DTW, -1, 0)  
    DTW_cut = np.delete(DTW_cut, 0, 1)
    diff = DTW_cut-dist
    warp_point = (0, len(n1)-1)
    path = []
    path.append(warp_point)
    while warp_point != (len(n1)-1,0):
        diff_element = diff[get_index(warp_point)]
        row = warp_point[0]
        column = warp_point[1]
        prev_candidates = [((DTW[get_index(point)][0]-diff_element[0])**2,point) for point in [(row+1, column+1), (row+1, column), (row, column)]]
        diff_prev = [n[0] for n in prev_candidates]
        prev = [tup[1] for tup in prev_candidates if tup[0]==min(diff_prev)][0] 
        prev = (prev[0],prev[1]-1)
        path.append(prev)
        warp_point=prev
    return path

def DTWDistance(DTW, s1, s2):
    return np.sqrt(DTW[0, len(s2)-1]) 

def DTW_kmeans(samplesize):#k, samplesize):
    k = 5
    start = time.time()
    data = pkl.load(open("../Data/normsynssh5_samp/normsynssh5_samp%s.pkl"%samplesize))
    choices = [data[label][data[label].keys()[0]].values() for label in data.keys()]
    centroids = []
    for n in range(k):
        random_seed = random.choice(choices)
        centroids.append(random_seed) 
        choices.remove(random_seed) 
    print "Clustering.."
    clusters = clustering(centroids, data, distance = "DTW")
    out = open("../Data/kmeans/DTW/one_gram%skDTW_samp%s.pkl" %(k,samplesize), "wb")   
    pkl.dump(clusters, out)
    out.close()
    end=time.time()
    out1 = open("../Data/kmeans/DTW/timek%s_s%s.pkl"%(k,samplesize), "wb")
    dct = {"time": (end/60.)-(start/60.), "k":k, "samplesize":samplesize}
    pkl.dump(dct, out1)
    out1.close()



def pick_centroids(k_clusters,data, distance="euclidean"):
    choices = [data[label][data[label].keys()[0]].values() for label in data.keys()]
    centroids = []
    initial = random.choice(choices)
    centroids.append(initial)
    choices.remove(initial)
    counter = 0
    print counter, str(datetime.datetime.now())
    for n in range(k_clusters-1):
        candidates = {}
        for datum in choices:
            dist = {}
            for centroid in centroids:
                if distance == "euclidean":
                    dist[tuple(centroid)] = euclid_dist(centroid, datum)
                else:
                    DTW = DTWarping(point, datum)[0]
                    dist[tuple(centroid)] = DTWDistance(DTW, point, datum)
            score = min(dist.values())
            candidates[tuple(datum)] = score
        chosen = [list(k) for k,v in candidates.iteritems() if v==max(candidates.values())][0]
        centroids.append(chosen)
        choices.remove(chosen)
        counter+=1
        print counter, str(datetime.datetime.now())
    return centroids


def kmeans(k):
    data = pkl.load(open("../Data/normsynssh5_cut.pkl"))
    centroids = pick_centroids(k, data)
    clusters = clustering(centroids, data)
    out = open("../Data/Revisions/Euclidean/k%s.pkl" %(k), "wb")
    pkl.dump(clusters, out)
    out.close()


def update_centroid(timeseries, labels, dist = "euclidean"):
    points = [timeseries[label][timeseries[label].keys()[0]].values() for label in labels]

    if dist == "euclidean":
        centroid = [sum([p[n] for p in points])*1./len(points) for n in range(len(points[0]))]
    else:
        M = copy.copy(points[0])
        for n in range(1,len(points)):
            P_i = points[n]
            W = get_DTWPath(M,points[n])
            M_tiled, P_i = tile_timeseries(M, P_i)
            for m in range(len(M)): 
                W_m1 = [i for i in W if i[1]==m]
                Q = [P_i[x] for x in W_m1] 
                M[m] = M[m] + (sum(Q)*1./len(Q)) 
        M = np.array(M)
        centroid = list(M*1./len(points))
    return tuple(centroid)

def clustering(init_centroids, data, distance = "euclidean"):
    clusters = {}
    for centroid in init_centroids:
        clusters.setdefault(tuple(centroid), [])
    counter = 0
    print counter, str(datetime.datetime.now())
    centroids = clusters.keys()
    updated_centroids = []
    while set(centroids) != set(updated_centroids):
        pcount = 0
        centroids = clusters.keys()
        d = 0
        for label in data.keys():
            datum = data[label][data[label].keys()[0]].values()
            dist = {}
            for centroid in centroids:
                if distance=="euclidean":
                    dist[tuple(centroid)] = euclid_dist(centroid,datum)
                else:
                    DTW = DTWarping(centroid, datum)[0]
                    dist[tuple(centroid)] = DTWDistance(DTW, centroid, datum)
            cluster = random.choice([k for k,v in dist.iteritems() if v == min(dist.values())])
            if label not in clusters[cluster]:
                clusters[cluster].append(label)
            for centroid in clusters.keys():
                if label in clusters[centroid]:
                    if centroid != cluster:
                        pcount += 1
                        clusters[centroid].remove(label)
            d+=1
        for centroid in clusters.keys():
            if clusters[centroid]:
                updated_centroid = update_centroid(data, clusters[centroid], dist=distance)
                if centroid != updated_centroid:
                    clusters[updated_centroid] = clusters[centroid]
                    clusters = {key:value for key,value in clusters.iteritems() if key!= centroid}
            #else:
                #clusters.pop(centroid)
        updated_centroids = clusters.keys()
        print pcount, len(centroids), str(datetime.datetime.now())
        print set(centroids) == set(updated_centroids)
    return clusters

def generate_null(data, number):
    null_set = copy.copy(data)
    counter = 0
    for syns in data.keys():
        years = data[syns][data[syns].keys()[0]].keys()
        vals = []
        for year in years:
            vals.append(random.uniform(0,1))
        np.random.shuffle(vals)
        null_set[syns][null_set[syns].keys()[0]] = dict(zip(years, vals))
        counter+=1
        print counter, str(datetime.datetime.now())
    out = open("../Data/kmeans/null_uniform/uniform_%s.pkl"%(number), "wb")
    pkl.dump(null_set, out)
    out.close()


def kmeans_null(null_no):
    null_set = pkl.load(open("../Data/kmeans/null_uniform/uniform_%s.pkl"%(null_no)))
    klist  = range(2,32)
    for k in klist:
        clusters = kmeans(k, null_set, null=True)
        out = open("../Data/kmeans/null_uniform/k%s_uniform%s.pkl"%(k,null_no), "wb")
        pkl.dump(clusters, out)



#null_list = range(5,11)
#samplelist = range(100,1000,50)
k= range(2,30)
if __name__ == '__main__':
    p = Pool(processes=7)
    p.map(kmeans, k)



#for k in range(5,16, 5):
#dct = {}
#for samplesize in range(10,500,50):
#    dct[samplesize] = DTW_kmeans(4,samplesize)
#out = open("../Data/kmeans/DTW/runtime_sampk%s.pkl"%(10), "wb")
#pkl.dump(dct, out)
#out.close()

