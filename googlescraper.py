import subprocess
import re
import bs4
import requests
import glob


r = requests.get('http://storage.googleapis.com/books/ngrams/books/datasetsv2.html')
soup = bs4.BeautifulSoup(r.text)
links = soup.find_all('a', href = re.compile('eng-all-2gram-20120701'))


already_dled = glob.glob("../Data/google-ngram/2-grams/*")

for link in links:
    download_link = link['href']
    filename = "../Data/google-ngram/2-grams/" + download_link[49:]
    if filename not in already_dled:
        subprocess.call('wget {0}'.format(download_link).split())

